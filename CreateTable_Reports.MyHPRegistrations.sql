USE [AvatarDW]
GO

DROP TABLE [Reports].[MyHpRegistrations]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Reports].[MyHpRegistrations](
	[Name] [varchar](100) NULL,
	[RegType] [varchar](100) NULL,
	[Email] [varchar](100) NULL,
	[RegisteredOn] [datetime] NULL,
	[NumLogins] [int] NULL,
	[LastLogin] [datetime] NULL,
	PATID varchar(12) NULL
) ON [PRIMARY]
GO


