
USE [AvatarDW]
GO

DROP PROCEDURE [Reports].[JC_A_SP_Create_Recent_Visit_Rpt_WithTracking]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Melody J Bishop
-- Create date: 2018-01-18
-- Description:	Create Recent Visit 
-- Change:      Update logic to remove duplicates and Add Tracking so we do not repeat for a week
-- Modified: 7/30/2018 MB - Removing dependence upon email in tracking and now using PATID as originally suggested.
--Modified: 8/2/2018 MB to exclude the 'NTST pending approval' message in Status
-- =============================================
CREATE PROCEDURE [Reports].[JC_A_SP_Create_Recent_Visit_Rpt_WithTracking] 

--          exec [Reports].[JC_A_SP_Create_Recent_Visit_Rpt_WithTracking]
AS
BEGIN
	--SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @sql varchar(max)
	set @sql=
	'
Declare @i int;
Declare @daysback int;
Declare @RangeBeg int;
Declare @RangeEnd int;
Declare @DaysAppts int;
Declare @CompareDays int;
	Set @daysback = -30  --default 8,but upping to 30 so we have it if they want to broaden the range-- rule was to delete rows after 8 days 
	Set @RangeBeg = -3   --default 3, to capture back the last 3 days information to 
	Set @RangeEnd = 0    --default 0, to capture todays appointment clients
	Set @CompareDays = -8 ---to capture back one weeks information to avoid duplicates
	--running as of 19th
-------------------------How many rows are old enough to delete from tracking-----------------------------------
Select @i = count(*) from Reports.MyHPSubmissions where Dateadd(dd,Datediff(d,0,DateOfAppt),0) < DateAdd(dd,DateDiff(d,-1*@daysBack,GetDate()),0)
--select @i
Print Convert(Varchar(10),@i) + '' Number of rows in table older than '' + Convert(Varchar(10),@DaysBack) +'' days''
Delete from Reports.MyHPSubmissions where Dateadd(dd,Datediff(d,0,DateOfAppt),0)  < DateAdd(dd,DateDiff(d,-1*@daysBack,GetDate()),0)

--select * from Reports.MyHPSubmissions order by datesenttonavigation

--Check for records dated today and delete then
--Load table

Delete from Reports.MyHPSubmissions 
	where DateAdd(dd,DateDiff(d,0,DateSentToNavigation),0)=DateAdd(dd,DateDiff(d,0,GetDate()),0)

Insert into Reports.MyHPSubmissions 
			  (patid,episode_number,patient_name_last,patient_name_first,date_of_birth,age,client_email_addr,status_value,DateSentToNavigation,DateOfAppt)
		Select patid,episode_number,patient_name_last,patient_name_first,date_of_birth,age,client_email_addr,status_value
					,DateAdd(dd,Datediff(dd,@RangeEnd,GetDate()),0), Appointment_date
			From 
			(select distinct
			  ad.patid ,ad.episode_number ,pcd.patient_name_last ,pcd.patient_name_first ,pcd.date_of_birth ,datediff(yy,pcd.date_of_birth,getdate()) as age
			 ,pcd.client_email_addr ,ad.status_value ,ad.appointment_date
				from 
				(Select patid, facility, status_code,Min(appointment_date+appointment_start_time) appointment_date
					from system.appt_data where appointment_date between dateadd(dd,Datediff(d,0,getdate()),@RangeBeg) and DateAdd(dd,DateDiff(d,0,getdate()),@RangeEnd)
					and	status_code not in(5,6,''NTST_PendingApproval'')
					Group by patid, facility,status_code
					)ad_1
				inner join 
				(Select patid, facility,staffid,episode_number, status_code, status_value, appointment_date,
					Appointment_Date+appointment_start_time ApptDate , appointment_start_time
					from system.appt_data where appointment_date between dateadd(dd,Datediff(d,0,getdate()),@RangeBeg) and DateAdd(dd,DateDiff(d,0,getdate()),@RangeEnd)
					and	status_code not in(5,6,''NTST_PendingApproval'')
				) ad 
					on  ad_1.patid     = ad.patid
					and ad_1.facility  = ad.facility
					and ad_1.appointment_Date = ad.Appointment_Date+ad.appointment_start_time
				--Updates here to join on PATID now instead of email			
				inner join (Select dem.patid, facility, client_email_addr,patient_name_last, patient_name_first, date_of_birth 
					from system.patient_current_demographics dem 
					left join reports.myhpregistrations mhp
					--on dem.client_email_addr = mhp.email
					on dem.patid = mhp.patid
					--and (dem.patient_name_first + '' '' + dem.Patient_name_last) = mhp.name
					where client_email_addr is not null 
					and client_email_addr <>''''
					and client_email_addr like ''%@%.%''
					--and mhp.email is null
					and mhp.patid is null
					) pcd --Enhance REGEX tbd
					on pcd.patid = ad.patid
					and pcd.facility = ad.facility
				left join (Select patid 
							from Reports.MyHpSubmissions 
							Where datesenttonavigation >= dateadd(dd,Datediff(d,0,getdate()),@CompareDays)
							)mhp
					on ad_1.patid=mhp.patid
			where 
			datediff(yy,pcd.date_of_birth,getdate()) >= 18
			and ad.appointment_date between  dateadd(dd,Datediff(d,0,getdate()),@RangeBeg) and DateAdd(dd,DateDiff(d,0,getdate()),@RangeEnd)
			and mhp.patid is null
			) sub

	Select * from Reports.MyHPSubmissions 
	where datesenttonavigation = DateAdd(d,0,DateDiff(dd,0,GetDate()))
		'

	
-- send report a email
--EXEC jc_a_sp_emailtojcmh 'melodyb',@sql,'RecentVisits_Withtracking.csv','MyHP Recent Visits With Tracking'
EXEC jc_a_sp_emailtojcmh 'MainOperators',@sql,'RecentVisits_WithTracking.csv','MyHP Recent Visits With Tracking'

END

GO


