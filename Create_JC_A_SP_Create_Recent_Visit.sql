USE [AvatarDW]
GO

/****** Object:  StoredProcedure [dbo].[JC_A_SP_Create_Recent_Visit_Rpt]    Script Date: 1/18/2018 9:38:20 AM ******/
DROP PROCEDURE [dbo].[JC_A_SP_Create_Recent_Visit_Rpt]
GO

/****** Object:  StoredProcedure [dbo].[JC_A_SP_Create_Recent_Visit_Rpt]    Script Date: 1/18/2018 9:38:20 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Peter L. Dunlap
-- Create date: 2017-06-15
-- Description:	Create Recent Visit Report
-- =============================================
CREATE PROCEDURE [dbo].[JC_A_SP_Create_Recent_Visit_Rpt] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @sql varchar(max)
	set @sql='
	select
	 distinct
	 ad.patid
	 ,ad.episode_number
	 ,pcd.patient_name_first
	 ,pcd.patient_name_last
	 ,pcd.date_of_birth
	 ,datediff(yy,pcd.date_of_birth,getdate()) as age
	 ,pcd.client_email_addr
	 ,ad.appointment_date
	 ,scd.name
	from system.appt_data ad
	join system.staff_current_demographics scd on scd.staffid = ad.staffid
	join system.billing_guar_data bgd on bgd.patid = ad.patid and bgd.episode_number = ad.episode_number
	join system.patient_current_demographics pcd on pcd.patid = ad.patid
	where 
	datediff(yy,pcd.date_of_birth,getdate()) >= 18
	and
	pcd.client_email_addr is not null and pcd.client_email_addr <> ''''
	and 
	ad.appointment_date between dateadd(dd,-3,getdate()) and getdate()
	and
	ad.status_code not in(5,6)
	'
	-- send report a email
	--EXEC jc_a_sp_emailtojcmh 'peterd',@sql,'RecentVisits.csv','Recent Visits'
	EXEC jc_a_sp_emailtojcmh 'MainOperators',@sql,'RecentVisits.csv','Recent Visits'


END

GO


