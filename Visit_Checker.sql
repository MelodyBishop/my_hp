--check the last few days for duplicate submissions
--look at what other days these submitted today were originally sent.
Select m.* from 
[AvatarDW].[Reports].[MyHpSubmissions] m
inner join 
(select patid, Count(*) ctr  FROM [AvatarDW].[Reports].[MyHpSubmissions] 
where --datesenttonavigation >= DateAdd(dd,0,Datediff(dd,0,GetDate()))
patid in (select patid   FROM [AvatarDW].[Reports].[MyHpSubmissions] 
where datesenttonavigation >= DateAdd(dd,0,Datediff(dd,0,GetDate()))
)
group by patid
Having count(*)>1
) sub
on m.patid=sub.patid
order by m.patid, datesenttonavigation
------------------------------------------------------------

--today's visit list
Select * from 
 [Reports].[MyHpSubmissions] 
 where  DateSenttoNavigation= DateAdd(dd,-0,DateDiff(d,-0,GetDate()))

--Check for count of appts by day
SELECT  count(*) ctr    ,[DateOfAppt]--, datesenttonavigation
  FROM [AvatarDW].[Reports].[MyHpSubmissions]
  group by dateofappt--,datesenttonavigation  
  order by dateofappt

--check for duplicate Patids WHOLE FILE
  SELECT  count(*) ctr   ,patid
  FROM [AvatarDW].[Reports].[MyHpSubmissions]
  group by patid   order by ctr desc

--check for duplicate Patids
  SELECT  count(*) ctr   ,patid
  FROM [AvatarDW].[Reports].[MyHpSubmissions]
  where dateofappt >= DateAdd(dd,-9,GetDate())
  group by patid   order by ctr desc


  --email regex - may use '%.@%.%' to start
  Select client_email_addr  
    FROM [AvatarDW].[Reports].[MyHpSubmissions]
 where client_email_addr like '^[a-zA-Z_]\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$'


--check for duplicate Patids--troubleshoot past 9 days
Select patid, datesenttonavigation,dateofappt
 from [AvatarDW].[Reports].[MyHpSubmissions]
--where patid in (
 -- SELECT  patid
 -- FROM [AvatarDW].[Reports].[MyHpSubmissions]
 where dateofappt = '1/25/2018'
  group by patid   
  having count(*)=2) 
  order by dateofappt
