USE [AvatarDW]
GO

DROP TABLE [Reports].[MyHpSubmissions]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Reports].[MyHpSubmissions](
	[Patid] [varchar](10) NOT NULL
	,[Episode_number] varchar(10) NULL
	,patient_name_last varchar(100) NULL
	,patient_name_first varchar(100) NULL
	,date_of_birth datetime NULL
	,age int NULL
	,client_email_addr varchar(200) NULL
	,status_value varchar(20) NULL
	,DateSentToNavigation datetime Not NULL
	,DateOfAppt datetime Not NULL
) ON [PRIMARY]
GO


